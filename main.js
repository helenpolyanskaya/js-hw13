// 1) setTimeout - повтор ф-ції разово через певний час;
//    setInterval - повтор ф-ції постійно через певниц час;

// 2) Так, вона спрацює миттєво, тому що таймер дорівнює 0;

// 3) ClearInterval() останавлює виконання циклу;


let image = document.querySelectorAll('.image-to-show');
let imageSelected;
let btnStop = document.getElementById('buttonStop');
let btnPlay = document.getElementById('buttonPlay');
let workingStop = true;
let currentImg = '0';

let timersArraay = []

function showImage(a) {
    hideAllImages();
    for (let i=a; i<image.length; i++) {
        console.log(i)
        let tId =setTimeout(() => {
            hideAllImages();
            image[i].style.display = ''; currentImg = i;
        }, (i - a)*3000 )
        timersArraay[i] = tId
    }
}

function hideAllImages() {
    for (let i=0; i<image.length; i++) {
        imageSelected = image[i]
        imageSelected.style.display = "none"; 
    }
}

showImage(0);

let showImg = setInterval(() => showImage(0), 12000);

btnStop.onclick = function() {
    for (timerId of timersArraay) {    
        clearTimeout(timerId);
    }   
    clearInterval(showImg);
}

btnPlay.onclick = function(){
   showImage(currentImg+1);
   setTimeout(() => {
    showImage(0);
    showImg = setInterval(() => showImage(0), 12000)
   }, (3 - currentImg)*3000 )   
}

